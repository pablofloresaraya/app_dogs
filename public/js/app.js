$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

jQuery().ready(function (){
    getDogs();
    $(".container").hide();
    $('#loader').hide();
})

function sendDog(){

    let nombre  =   $('#nombre').val();
    let color   =   $('#color').val();
    let raza    =   $('#raza').val();

    let href = $(".btn-submit").attr('data-attr');
    
    $.ajax({
        url:href,
        data:{nombre:nombre, color:color, raza:raza},
        dataType: "json", 
        method:"POST",

        success:function(data){ 

            if(data.fails){
                let msj = "";
                if(data.errors.nombre){
                    msj=data.errors.nombre
                }else if(data.errors.color){
                    msj=data.errors.color
                }else{
                    msj=data.errors.raza
                }
                $("#msj").text(msj);
                $("#modalAlert").modal("show");
            }else{
                if(!data.status){                
                    getDogs();
                }else{
                    $("#msj").text(data.msj);
                    $("#modalAlert").modal("show");
                }                
            }

       },
       error: function (data) {
            alert('Mensaje de sistema', 'Ha ocurrido un error en el sistema<br />'+data.responseText);
        }

    })
}    

function getDogs(){

    let href = $("#form").attr('data-attr');
    
    $.ajax({
        url:href,
        //data:{},
        dataType: "json", 
        method:"get",
        beforeSend: function() {
            $('#loader').html('<img src=imagenes/loading.gif>');
        },
        success:function(data){
            
            let html="";

            if(data.dogs.length>0){          
                
                for(let i=0; i<data.dogs.length;i++){

                    let index=i+1;

                    html+= "<tr id="+data.dogs[i].id+">";
                        html+="<td>"+index+"</td>";
                        html+="<td id='nombre_"+data.dogs[i].id+"'>"+data.dogs[i].nombre+"</td>";
                        html+="<td id='color_"+data.dogs[i].id+"'>"+data.dogs[i].color+"</td>";
                        html+="<td id='raza_"+data.dogs[i].id+"'>"+data.dogs[i].raza+"</td>";
                        html+="<td id='idestado_"+data.dogs[i].id+"' style='display:none'>"+data.dogs[i].estado+"</td>";
                        html+="<td id='estado_"+data.dogs[i].id+"'>"+data.dogs[i].estado+"</td>";
                        html+="<td class='text-center'><button type='button' class='btn btn-sm btn-primary' onclick='editModal("+data.dogs[i].id+")'>Modificar</button></td>"
                        html+="<td class='text-center'><button type='button' class='btn btn-sm btn-danger' onclick='confModal("+data.dogs[i].id+")'>Eliminar</button></td>"
                    html+= "</tr>";

                }

                $(".container").show();
                $("#nombre").val("");
                $("#color").val("");
                $("#raza").val("");

            }else{
                $(".container").hide();
            }

            $("#perritos").html(html);            


    },
    complete: function() {
        $('#loader').hide();
    },
    error: function (data) {
        alert('Mensaje de sistema', 'Ha ocurrido un error en el sistema<br />'+data.responseText);
    }

    });

}

function confModal(id){
    $("#clave").val(id);
    $("#modalConf").modal('show');
}

function deleteDog(){
    
    let id = $("#clave").val();
    let href = "dog/"+id;

    $.ajax({
        url:href,
        data:{id:id},
        dataType: "json", 
        type:"DELETE",

        success:function(data){ 
            
            if(data.status){ 
                getDogs();                
                $("#modalConf").modal('hide');
            }else{
                console.log('error');
            }

       },
       error: function (data) {
            alert('Mensaje de sistema', 'Ha ocurrido un error en el sistema<br />'+data.responseText);
        }

    })
}

function editModal(id){

    let html="";

    let nombre = $("#nombre_"+id).text();
    let color  = $("#color_"+id).text();
    let raza   = $("#raza_"+id).text();
    let estado   = $("#estado_"+id).text();  

    $("#e_clave").val(id);
    $("#e_nombre").val(nombre);
    $("#e_color").val(color);
    $("#e_raza").val(raza);
    $("#e_msg").hide();

    let pen_selected = "";
    let rev_selected = "";
    let rec_selected = "";

    switch (estado) {

        case "Pendiente":

        pen_selected="selected";

        break;
    
        case "Revision":

        rev_selected="selected";

        break;

        case "Recuperado":

        rec_selected="selected";

        break;

    }       
            
    html+= "<option value='Pendiente' "+pen_selected+">Pendiente</option>";
    html+= "<option value='Revision' "+rev_selected+">Revision</option>";
    html+= "<option value='Recuperado' "+rec_selected+">Recuperado</option>";   

    $("#e_estado").html(html);
    $("#modalEdit").modal('show');
    
}

function editDog(){

    $("#e_msg").hide();

    let id      = $("#e_clave").val();
    let nombre  = $("#e_nombre").val();
    let color   = $("#e_color").val();
    let raza    = $("#e_raza").val();
    let estado  = $("#e_estado").val();
    let href    = "dog/"+id;

    $.ajax({
        url:href,
        data:{id:id, nombre:nombre, color:color, raza:raza, estado:estado},
        dataType: "json", 
        type:"PUT",

        success:function(data){
           
            if(data.fails){

                let msj = "";
                if(data.errors.nombre){
                    msj=data.errors.nombre
                }else if(data.errors.color){
                    msj=data.errors.color
                }else{
                    msj=data.errors.raza
                }

                $("#e_msj").text(msj);
                $("#e_msg").show();

            }else{
            
                if(data.status){
                    getDogs();                
                    $("#modalEdit").modal('hide');
                }else{
                    let msj = data.msg;
                    $("#e_msj").text(msj);
                    $("#e_msg").show();
                }

            }

       },
       error: function (data) {
            alert('Mensaje de sistema', 'Ha ocurrido un error en el sistema<br />'+data.responseText);
        }

    })

}




