@extends('layouts.plantilla')

@section('title','Perritos')

@section('contents')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1><u>Ingreso de Perrito</u></h1>
            </div>
        </div>
        <br>
        <div id="form" class="container-fluid" data-attr="{{route('dog.show')}}">
            
            <div class="row">
                <div class="col-sm-12">
                    <label class="text-right col-sm-1 col-sm-offset-1">Nombre:</label>
                    <div class="col-sm-2"><input name="nombre" id="nombre" type="text" value=""></div>

                    <label class="text-right col-sm-1">Color:</label>
                    <div class="col-sm-2"><input name="color" id="color" type="text" value=""></div>

                    <label class="text-right col-sm-1">Raza:</label>
                    <div class="col-sm-2"><input name="raza" id="raza" type="text" value=""></div>

                    <div class="col-sm-1">
                        <button type="button" class="btn btn-sm btn-success btn-submit" data-attr="{{route('dog.store')}}" onclick="sendDog()">Guardar</button>
                    </div>
                </div>
            </div>
            <br>
            <div class="container">
                <div id="loader"></div>
                <table id="dogs" class="table table-striped table-hover">
                    <thead>                    
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Color</th>
                        <th>Raza</th>
                        <th>Estado</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </thead>
                    <tbody id="perritos">
    
                    </tbody>        
                </table>    
            </div>    
            
        </div>
    </div>

    <div class="modal fade" id="modalConf" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-danger"> 
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-title">Eliminar</h4> 
                </div>
                <div class="modal-body" id="mediumBody">
                    <div class="container">
                        Est&aacute; seguro de eliminar el regitro?
                    </div>
                    <input id="clave" type="hidden">
                </div>
                <div class="modal-footer">
                    <div class="row">                                               
                        <div class="col-sm-2 col-sm-offset-8">                 
                            <button type="button" class="btn btn-success btn-sm" onclick="deleteDog()">Aceptar</button>
                        </div>
                        <div class="col-sm-2">                 
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
                        </div>      
                    </div>
                </div>    
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
    aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-primary">                    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>    
                    <h4 class="modal-title" id="modal-title">Modificar</h4>                                    
                </div>
                <div class="modal-body" id="mediumBody">
                    <div class="col-sm-12 container-fluid">
                        <input id="e_clave" type="hidden">
                        <div id="e_msg" class="row">
                            <div id="e_msj" class="text-center text-danger"></div>
                        </div>
                        <div class="row">
                            <label class="text-right col-sm-2">Nombre:</label>
                            <div class="col-sm-5"><input name="e_nombre" id="e_nombre" type="text" value=""></div>
                        </div>
                        <div class="row">
                            <label class="text-right col-sm-2">Color:</label>
                            <div class="col-sm-5"><input name="e_color" id="e_color" type="text" value=""></div>
                        </div>
                        <div class="row">
                            <label class="text-right col-sm-2">Raza:</label>
                            <div class="col-sm-5"><input name="e_raza" id="e_raza" type="text" value=""></div>
                        </div>
                        <div class="row">
                            <label class="text-right col-sm-2">Estado:</label>
                            <div class="col-sm-5">
                                <select id="e_estado" style="width:100%">
                                    
                                </select>    
                            </div>        
                        </div>    
                    </div>                    
                </div>
                <div class="modal-footer">
                    <div class="row">                                               
                        <div class="col-sm-2 col-sm-offset-8">                 
                            <button type="button" class="btn btn-success btn-sm" onclick="editDog()">Guardar</button>
                        </div>
                        <div class="col-sm-2">                 
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancelar</button>
                        </div>      
                    </div>
                </div>    
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="modalAlert" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
    aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header modal-header-danger"> 
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-title">Alerta</h4> 
                </div>
                <div class="modal-body" id="mediumBody">
                    <div class="container">
                        <div id="msj" class="col-sm-12"></div>
                    </div>
                    <input id="clave" type="hidden">
                </div>
                <div class="modal-footer">
                    <div class="row">                                               
                        <div class="col-sm-2 col-sm-offset-10">                 
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Aceptar</button>
                        </div>      
                    </div>
                </div>    
            </div>
        </div>
    </div>

@endsection