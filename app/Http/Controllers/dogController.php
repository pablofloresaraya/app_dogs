<?php

namespace App\Http\Controllers;

use App\Models\Perrito;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Register\SaveDog;
use App\Register\GetLista;
use App\Register\EditDog;
use App\Register\DeleteDog;

class dogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SaveDog $create)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'color' => 'required',
            'raza' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['fails'=>$validator->fails(),'errors'=>$validator->errors()]);
        }

        return $create->dogSave($request);

            /*$now = Carbon::now(); //fecha actual
            $now->format('d-m-Y');
            $edad=Carbon::createFromDate(2021,01,28)->age; //calcula edad*/

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(GetLista $show)
    {          
        return $show->ListaGet();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, EditDog $edit)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'color' => 'required',
            'raza' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['fails'=>$validator->fails(),'errors'=>$validator->errors()]);
        }

        return $edit->dogEdit($request,$id);
 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, DeleteDog $delete)
    {

        return $delete->dogDelete($id);

    }
}
