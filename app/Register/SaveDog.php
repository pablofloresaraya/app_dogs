<?php

namespace App\Register;

use App\Models\Perrito;

class SaveDog
{
    public function dogSave($request)
    {
        $valida = Perrito::where("nombre",$request->nombre)->count();

        if($valida == 0){

            $perrito = new Perrito();

            $perrito->nombre = $request->nombre;
            $perrito->color  = $request->color;
            $perrito->raza   = $request->raza;
            $perrito->estado = "Pendiente";
            
            $perrito->save();

            return response()->json(['status'=>0]);

        }else{
            return response()->json(['status'=>1,'msj'=>'El nombre ingresado ya existe.']);
        }
    }
}
