<?php

namespace App\Register;

use App\Models\Perrito;

class GetLista
{
    public function ListaGet(){

        $perritos = Perrito::select('id','nombre','color','raza','estado')->orderBy('id','desc')->get();     
        return response()->json(['dogs'=>$perritos]);

    }

}