<?php

namespace App\Register;

use App\Models\Perrito;

class DeleteDog
{
    public function dogDelete($id){

        $res=Perrito::find($id)->delete();
        if ($res){
            $data=[
            'status'=>1,
            'msg'=>'success'
          ];
        }else{
            $data=[
            'status'=>0,
            'msg'=>'fail'
          ];
        }
        
        return response()->json($data);

    }

}