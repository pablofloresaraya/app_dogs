<?php

namespace App\Register;

use App\Models\Perrito;
use Illuminate\Support\Facades\DB;

class EditDog
{
    public function dogEdit($request,$id)
    {   
        
        $iden = Perrito::where("nombre",$request->nombre)->value("id");

        if($iden==$id){ //modifica color y/o raza

            $res = Perrito::where("id",$id)->update($request->all());

            if ($res){
                $data=[
                'status'=>1,
                'msg'=>'success'
            ];
            }else{
                $data=[
                'status'=>0,
                'msg'=>'fail'
                ];
            }
            
            return response()->json($data);

        }else{ //modifica nombre

            $conta=Perrito::where("nombre",$request->nombre)->count();

            if($conta==0){

                $res = Perrito::where("id",$id)->update($request->all());
                if ($res){
                    $data=[
                    'status'=>1,
                    'msg'=>'success'
                ];
                }else{
                    $data=[
                    'status'=>0,
                    'msg'=>'fail'
                    ];
                }
                return response()->json($data);

            }else{

                return response()->json(['status'=>0,'msg'=>'*El nombre ingresado ya existe.']);

            }

        }

    }

}