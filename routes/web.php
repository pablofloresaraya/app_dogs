<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\dogController;
use App\Http\Controllers\estadoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [dogController::class, 'index']);

Route::post('dog', [dogController::class, 'store'])->name('dog.store');

Route::get('dog/show', [dogController::class, 'show'])->name('dog.show');

Route::put('dog/{id}', [dogController::class, 'update'])->name('dog.update');

Route::delete('dog/{id}', [dogController::class, 'destroy'])->name('dog.destroy');

Route::get('estado/show', [estadoController::class, 'show'])->name('estado.show');

